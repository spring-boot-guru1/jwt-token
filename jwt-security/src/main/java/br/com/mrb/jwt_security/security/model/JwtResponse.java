package br.com.mrb.jwt_security.security.model;

import java.io.Serializable;

/**
 * 
 * @author mauro
 * @see  - Guarda o token jwt que vai ser enviado para o client
 */
public class JwtResponse implements Serializable {

	private static final long serialVersionUID = -8091879091924046844L;
	private final String jwttoken;

	public JwtResponse(String jwttoken) {
		this.jwttoken = jwttoken;
	}

	public String getToken() {
		return this.jwttoken;
	}

}